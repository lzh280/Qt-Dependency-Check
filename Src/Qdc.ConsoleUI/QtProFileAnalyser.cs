﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Qdc.ConsoleUI
{
        public class QtProFileAnalyser
        {
                public QtProFileAnalyser(string filePath)
                {
                        _filePath = filePath;
                }


                public List<string> Analyse()
                {
                        var lines = File.ReadAllLines(_filePath);

                        List<string> result = new List<string>();

                        foreach (var line in lines)
                        {
                                if (!line.StartsWith("DEPENDPATH"))
                                {
                                     continue;   
                                }

                                var lib = line.Split(new []{@"/"}, StringSplitOptions.RemoveEmptyEntries).Last();

                                if (!result.Contains(lib))
                                {
                                        result.Add(lib);
                                }
                                else
                                {
                                        Console.WriteLine("重复依赖：" + lib);
                                }
                        }
                        
                        return result;
                }

                private readonly string _filePath;
        }
}
