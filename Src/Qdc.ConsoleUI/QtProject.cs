﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Qdc.ConsoleUI
{
        public class QtProject
        {
                public QtProject(string dirPath)
                {
                        DirPath = dirPath;
                        //Console.WriteLine("项目目录：" + DirPath);

                        var di = new DirectoryInfo(DirPath);
                        Name = di.Name;
                        

                        ProFilePath = Path.Combine(DirPath, Name + ".pro");
                        //Console.WriteLine("项目文件：" + ProFilePath);
                }

                public void FileAnalyse()
                {
                        var analyser = new QtProFileAnalyser(ProFilePath);
                        FileDependencies.Clear();
                        FileDependencies.AddRange(analyser.Analyse());
                }

                

                public string DirPath { get; }

                public string Name { get; }

                public string ProFilePath { get; }

                public List<string> FileDependencies { get; } = new List<string>();

                public List<string> AnalyseDependencies { get; } = new List<string>();
        }
}