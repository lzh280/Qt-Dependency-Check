﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Qdc.ConsoleUI
{
        public class QtSolution
        {
                public QtSolution(string dirPath)
                {
                        DirPath = dirPath;
                }

                public void ScanProjects()
                {
                        QtProjects.Clear();

                        var dirPaths = Directory.GetDirectories(DirPath,"*", SearchOption.TopDirectoryOnly);

                        foreach (var dirPath in dirPaths)
                        {
                                QtProject qtProject = new QtProject(dirPath);
                                QtProjects.Add(qtProject);
                        }

                        //foreach (var qtProject in QtProjects)
                        //{
                        //        var ds = GetAllDependencies(qtProject.Name);

                        //        qtProject.AnalyseDependencies.Clear();
                        //        qtProject.AnalyseDependencies.AddRange(ds);

                        //        qtProject.DisplayFileDependencies();
                        //        qtProject.DisplayAnalyseDependencies();

                        //        var except = qtProject.FileDependencies.Except(qtProject.AnalyseDependencies);
                        //        foreach (var item in except)
                        //        {
                        //                Console.WriteLine("依赖差异：" + item);
                        //        }
                        //}
                }

                public void FileAnalyse()
                {
                        foreach (var project in QtProjects)
                        {
                                project.FileAnalyse();
                        }
                }

                public void CascadeAnalyse()
                {
                        
                }

                private List<string> GetAllDependencies(string projectName)
                {
                        var result = new List<string>();
                        var project = QtProjects.FirstOrDefault(i => i.Name == projectName);

                        if (project == null || project.FileDependencies.Count == 0)
                        {
                                return result;
                        }

                        result.AddRange(project.FileDependencies);

                        foreach (var di in project.FileDependencies)
                        {
                                result.AddRange(GetAllDependencies(di));
                        }
                        return result.Distinct().ToList();
                }

                public void ScanExist()
                {
                        foreach (var qtProject in QtProjects)
                        {
                                var sub = qtProject.FileDependencies;

                        }
                }


                public string DirPath { get; private set; }

                public List<QtProject> QtProjects { get; } = new List<QtProject>();
        }
}