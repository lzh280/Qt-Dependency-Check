﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qdc.ConsoleUI
{
        class Program
        {
                static void Main(string[] args)
                {
                        QtSolution solution = new QtSolution(Environment.CurrentDirectory);

                        Console.WriteLine("解决方案目录：" + solution.DirPath);
                        Console.WriteLine();

                        solution.ScanProjects();

                        solution.FileAnalyse();
                        foreach (var project in solution.QtProjects)
                        {
                                DisplayProjectFileDependencies(project);
                                Console.WriteLine();
                        }


                        Console.ReadKey();
                }


                private static void DisplayProjectFileDependencies(QtProject project)
                {
                        Console.WriteLine("项目名称：" + project.Name);
                        foreach (var dependency in project.FileDependencies)
                        {
                                Console.WriteLine("文件依赖：" + dependency);
                        }
                }

                private static void DisplayProjectAnalyseDependencies(QtProject project)
                {
                        Console.WriteLine("项目名称：" + project.Name);
                        foreach (var dependency in project.AnalyseDependencies)
                        {
                                Console.WriteLine("分析依赖：" + dependency);
                        }
                }
        }
}
